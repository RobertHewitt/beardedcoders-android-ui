package com.beardedcoders.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.SherlockActivity;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 27/05/12
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseActivity
  extends SherlockActivity
  implements ViewState
{
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		onLoadState(getIntent(), savedInstanceState);
		onLoadViews();
	}

	@Override
	public void onLoadState( Intent intent, Bundle savedInstanceState )
	{
	}

	@Override
	public void onLoadViews()
	{
	}
}
