package com.beardedcoders.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 24/12/12
 * Time: 20:12
 * To change this template use File | Settings | File Templates.
 */
public interface ViewState {

    void onLoadState( Intent intent, Bundle savedInstanceState );
    void onLoadViews();
}
