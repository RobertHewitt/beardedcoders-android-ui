package com.beardedcoders.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragmentActivity;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 24/12/12
 * Time: 20:09
 * To change this template use File | Settings | File Templates.
 */
public abstract class FragmentActivity
  extends SherlockFragmentActivity
  implements ViewState
{
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		onLoadState(getIntent(), savedInstanceState);
		onLoadViews();
	}

	@Override
	public void onLoadState( Intent intent, Bundle savedInstanceState )
	{
	}

	@Override
	public void onLoadViews()
	{
	}
}
