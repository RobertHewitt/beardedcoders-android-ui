package com.beardedcoders.android.ui.adapter;

import android.widget.BaseAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 09/09/12
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public abstract class BCBaseAdapter<T> extends BaseAdapter implements Serializable
{
	private static final long serialVersionUID = 7686604991742389732L;
	private final ArrayList<T> data = new ArrayList<T>();

	public void add(List<T> items)
	{
		data.addAll(items);
		notifyDataSetChanged();
	}

	public void addItem( T item )
	{
		data.add(item);
		notifyDataSetChanged();
	}

	public void addItemAt( T item, int at )
	{
		data.add(at, item);
		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		return data.size();
	}

	@Override
	public T getItem( int i )
	{
		return data.get(i);
	}

	@Override
	public long getItemId( int i )
	{
		return i;
	}
}
