package com.beardedcoders.android.ui.view;


import com.beardedcoders.android.ui.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

public class SliderView extends View {
	
	private int sliderValueMinimum = 1;
	private int sliderValueMaximum = 100;
	private int sliderValueInterval = 1;
	
	public SliderView(Context context) {
		this(context, null);
	}
	
	public SliderView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
	public SliderView(Context context, AttributeSet attrs, int defStyle) {
		 super(context, attrs, defStyle);
		 setXMLDefaults(attrs, context);
	}
	
	private void setXMLDefaults(AttributeSet attrs, Context c)
	{
		try{
			TypedArray a = c.obtainStyledAttributes(attrs,R.styleable.PreferenceSeekBarXML);
			sliderValueMinimum = a.getInt(R.styleable.PreferenceSeekBarXML_sliderMin, sliderValueMinimum);
			sliderValueInterval = a.getInt(R.styleable.PreferenceSeekBarXML_increment, sliderValueInterval);
			sliderValueMaximum = a.getInt(R.styleable.PreferenceSeekBarXML_sliderMax, sliderValueMaximum) - sliderValueMinimum;
			
		}catch (Exception e){
			
		}
	}
	
	

}
