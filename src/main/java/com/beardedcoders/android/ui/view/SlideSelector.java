package com.beardedcoders.android.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.beardedcoders.android.utils.screen.UtilScreen;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SlideSelector extends LinearLayout{
	private List<String> optionStrings;
	private List<TextView> optionViews;
	private Context context;
	private int pxFor1Dp;
	private int pxFor3Dp;

	public SlideSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
		initVariables(context);
		createViewsFromStrings();
	}

	public SlideSelector(Context context) {
		super(context);
		initVariables(context);
		createViewsFromStrings();
	}

	/**
	 * variable initialiser. 
	 * This should be called from onCreate()
	 * Usually this wouldn't need to be called at any other time
	 * @param context
	 */
	private void initVariables(Context context) {
		optionStrings = new ArrayList<String>();
		optionViews = new ArrayList<TextView>();
		this.context = context;
		pxFor1Dp = UtilScreen.getPixelForDip(context, 1);
		pxFor3Dp = UtilScreen.getPixelForDip(context, 3);
	}
	
	/**
	 * This should be called from onCreate()
	 * Usually this wouldn't need to be called at any other time
	 * This will construct the text views required to hold the strings
	 * Order of the strings get applied 
	 */
	private void createViewsFromStrings() {
		
		if (optionViews.size() >= 1) {
			removeAllViews();
			optionViews.removeAll(optionViews);
		}
		
		for (String option : optionStrings) {
			addView(getTextView(option));
			addView(getDividerView());
		}		
	}

	private TextView getTextView(String option) {
		TextView toAdd = new TextView(context);
		toAdd.setText(option);
		optionViews.add(toAdd);
		return toAdd;
	}
	
	/**
	 * This will clear out any existing options and internally 
	 * set up the views required to render them on screen.  
	 * @param optionsToAdd
	 */
	public void setOptionsForUI(List<String> optionsToAdd) {
		this.optionStrings = optionsToAdd;
		createViewsFromStrings();
	}
	
	private View getDividerView() {
		View toReturn = new View(context);
		toReturn.setLayoutParams(new LayoutParams(pxFor1Dp, LayoutParams.FILL_PARENT));
		toReturn.setPadding(pxFor3Dp, 0, pxFor3Dp, 0);
		return toReturn;
	}


}
